const { user } = require('../src/util.js');

module.exports = (app) => {
    app.post('/', (req, res) => {
        return res.send({'data': {}})
    })

    app.post('/currency', (req, res) => {
        return res.send({
            currency : user
        })
    })

    app.post('/person', (req, res) => {
        //Name is missing
        if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error' : 'Bad Request - missing required parameter NAME'
            })
        }

        //Name is string
        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error' : 'Bad Request - Name has to be a string'
            })
        }

        if(typeof req.body.name !== ''){
            return res.status(400).send({
                'error' : 'Bad Request - Name has to be a empty'
            })
        }

        //Ex Parameter
        if(!req.body.hasOwnProperty('ex')){
            return res.status(400).send({
                'error' : 'Bad Request - missing required parameter EX'
            })
        }

        if(typeof req.body.ex !== 'object'){
            return res.status(400).send({
                'error' : 'Bad Request - Ex has to be a object'
            })
        }

        if(typeof req.body.ex !== ''){
            return res.status(400).send({
                'error' : 'Bad Request - Ex has to be a empty'
            })
        }

        if(!req.body.hasOwnProperty('alias')){
            return res.status(400).send({
                'error' : 'Bad Request - missing required parameter alias'
            })
        }

        if(typeof req.body.ex !== 'string'){
            return res.status(400).send({
                'error' : 'Bad Request - Alias has to be a string'
            })
        }

        if(typeof req.body.alias === ''){
            return res.status(400).send({
                'error' : 'Bad Request - Alis has to be a empty'
            })
        }

        if(typeof {ex : req.body.ex}.then(result => {
            if(result.length > 0) {
                return res.status(400).send({
                    'error' : 'Bad Request - no duplicate'
                })
            }
        }))

        if(typeof req.body.user){
            return res.status(200).send({
                'success' : 'Good Request - All fields no duplicate'
            })
        } 

    }) 
}