const user = {
    alias: 'riyadh',
	name: 'Saudi Arabian Riyadh',
	ex: {
		'peso': 0.47,
        'usd': 0.0092,
        'won': 10.93,
        'yuan': 0.065
	}
}

module.exports = {
    user
}