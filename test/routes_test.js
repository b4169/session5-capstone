const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('API Test Suite - Capstone', () => {
    it('[1] Check if post /currency is running', () => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            alias: 'anna',
	        name: 'Anna Blair Lopez',
            ex: {
                'peso': 0.48,
                'usd': 0.0093,
                'won': 10.94,
                'yuan': 0.066
            }
        })
        .end((err, res) => {
            expect(res).to.not.equal(undefined);
        })
    })

    it('[2] Check if post /currency returns status 400 if name is missing', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            alias: 'anna',
            ex: {
                'peso': 0.48,
                'usd': 0.0093,
                'won': 10.94,
                'yuan': 0.066
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    });

    it('[3] Check if post /currency returns status 400 if name is not a string', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Anna Blair Lopez'
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })
    
    it('[4] Check if post /currency returns status 400 if name is empty', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: '',
            alias: 'anna',
            ex: {
                'peso': 0.48,
                'usd': 0.0093,
                'won': 10.94,
                'yuan': 0.066
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[5] Check if post /currency returns status 400 if ex is missing ', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Anna Blair Lopez',
            alias: 'anna'
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[6] Check if post /currency returns status 400 if ex is not an object ', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Anna Blair Lopez',
            alias: 'anna',
            ex: {
                'peso': 0.48,
                'usd': 0.0093,
                'won': 10.94,
                'yuan': 0.066
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[7] Check if post /currency returns status 400 if ex is empty ', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Anna Blair Lopez',
            alias: 'anna',
            ex: {
                
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[8] Check if post /currency returns status 400 if alias is missing', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Anna Blair Lopez',
            ex: {
                'peso': 0.48,
                'usd': 0.0093,
                'won': 10.94,
                'yuan': 0.066
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[9] Check if post /currency returns status 400 if alias is not an string', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Saudi Arabian Riyadh',
            alias: 1,
            ex: {
                'peso': 0.47,
                'usd': 0.0092,
                'won': 10.93,
                'yuan': 0.065
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[10] Check if post /currency returns status 400 if alias is empty', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Anna Blair Lopez',
            alias: '',
            ex: {
                'peso': 0.48,
                'usd': 0.0093,
                'won': 10.94,
                'yuan': 0.066
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[11] Check if post /currency returns status 400 if ex all fields are complete but there is a duplicate alias', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Saudi Arabian Riyadh',
            alias: 'riyadh',
            ex: {
                
            }
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    })

    it('[12] Check if post /currency returns status 200 if all fields are complete and there are no dupicates', (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            name: 'Saudi Arabian Riyadh',
            alias: 'riyadh',
            ex: {
                'peso': 0.47,
                'usd': 0.0092,
                'won': 10.93,
                'yuan': 0.065
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(200);
            done();
        });
    })
})